class AuthS {
  int status;
  Message message;

  AuthS({this.status, this.message});

  AuthS.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message =
        json['message'] != null ? new Message.fromJson(json['message']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message.toJson();
    }
    return data;
  }
}

class Message {
  List<String> errorInfo;

  Message({this.errorInfo});

  Message.fromJson(Map<String, dynamic> json) {
    errorInfo = json['errorInfo'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['errorInfo'] = this.errorInfo;
    return data;
  }
}