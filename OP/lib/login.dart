import 'dart:convert';

import 'package:OP/models/authS.dart';
import 'package:OP/models/services.dart';
import 'package:OP/profile.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_messaging/firebase_messaging.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}



class _LoginState extends State<Login> {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  String _homeScreenText = "Waiting for token...";
  TextEditingController telController = new TextEditingController();

  @override
  void initState() {
    super.initState();

    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      setState(() {
        _homeScreenText = "$token";
      });
      print(_homeScreenText);
    });
  }

  auth(String phone, String _homeScreenText) async {
    final res = await http.post("${Services.auth}",
        body: {"mobile_no": phone, "token": _homeScreenText});
    if (res.statusCode == 200) {
      Map<String, dynamic> map = json.decode(res.body);
      String status = map["status"].toString();
      if (status == '1') {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  ProfilePage(phoneM: phone, tokenM: _homeScreenText)),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final fontA = TextStyle(
      fontFamily: 'Athiti',
      color: Colors.black,
      fontSize: 17,
    );
    final fontB = TextStyle(
      fontFamily: 'Athiti',
      color: Colors.white,
      fontSize: 17,
    );
     final fontC = TextStyle(
      fontFamily: 'Athiti',
      color: Colors.white,
      fontSize: 17,
    );
    return Scaffold(
      appBar: AppBar(
        title: Center(
            child: Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child:
              Image.asset("assets/img/OP.jpeg", fit: BoxFit.cover, width: 230),
        )),
        backgroundColor: Colors.white,
      ),
      body: Container(
        color: Color(0xff00aeac),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: telController,
                keyboardType: TextInputType.number,
                style: fontC,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                  enabledBorder: const OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Colors.white, width: 2.0),
                  ),
                  border: new OutlineInputBorder(
                      borderSide:
                          new BorderSide( width: 2)),
                  hintText: "กรุณาใส่เบอร์โทรศัพท์มือถือ",
                  hintStyle: fontB,
                ),
              ),
            ),
            RaisedButton(
              onPressed: () {
                var phone = telController.text;
                auth(phone, _homeScreenText);
              },
              child: Text(
                'เข้าสู่ระบบ',
                style: fontA,
              ),
            )
          ],
        ),
      ),
    );
  }
}
