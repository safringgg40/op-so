import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebPage extends StatefulWidget {
  final String linkm;

  WebPage({Key key, this.linkm}) : super(key: key);

  @override
  _WebPageState createState() => _WebPageState(link: linkm);
}

class _WebPageState extends State<WebPage> {
  String link;

  _WebPageState({Key key, this.link});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( iconTheme: IconThemeData(
    color: Colors.black, //change your color here
  ),
        title: Center(
            child: Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child:
              Image.asset("assets/img/OP.jpeg", fit: BoxFit.cover, width: 230),
        )),
        backgroundColor: Colors.white,
      ),
      body: WebView(
        initialUrl: '$link',
        // initialUrl: 'http:\/\/mobile.orientalprincess.com\/app\/images\/promotion\/shop\/1610590492Shoponline-JANSpecial-mobileapp.jpg',
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
  }
}
