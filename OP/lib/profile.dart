import 'dart:convert';

import 'package:OP/login.dart';
import 'package:OP/models/services.dart';
import 'package:OP/webview.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ProfilePage extends StatefulWidget {
  final String phoneM;
  final String tokenM;
  ProfilePage({Key key, this.phoneM, this.tokenM}) : super(key: key);

  @override
  _ProfilePageState createState() =>
      _ProfilePageState(phone: phoneM, token: tokenM);
}

class _ProfilePageState extends State<ProfilePage> {
  String phone;
  String token;
  String memNo;
  bool isLoading;
  String link;
  List<dynamic> map;
  int lenWeb;
  String name;
  String surname;
  String mobileNo;
  String point;
  String appId;

  TabController tabController; //tab

  TextEditingController telController = new TextEditingController();

  _ProfilePageState({Key key, this.phone, this.token});
  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    await getProfile(phone, token);
    await getUrl(phone, memNo);
  }

  getProfile(String phone, String token) async {
    final res = await http.post("${Services.profile}",
        body: {"mobile_no": phone, "token": token.toString()});
    if (res.body != '') {
      Map<String, dynamic> map = json.decode(res.body);
      memNo = map["member_no"].toString();
      name = map["name"];
      surname = map["surname"];
      mobileNo = map["mobile_no"];
      point = map["point"];
      appId = map["app_id"];

      setState(() {
        isLoading = false;
      });
    }
  }

  getUrl(String phone, String memNo) async {
    final res = await http.post("${Services.promotion}", body: {
      "mobile_no": phone,
      "token": token,
      "member_no": memNo.toString()
    });
    if (res.body != '') {
      map = json.decode(res.body.trim());
      lenWeb = map.length;
      print(res.body);

      setState(() {
        isLoading = false;
      });
    }
  }

  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(
        "ยกเลิก",
        style: TextStyle(
          fontFamily: 'Athiti',
        ),
      ),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = FlatButton(
      child: Text(
        "ออกจากระบบ",
        style: TextStyle(color: Colors.red, fontFamily: 'Athiti'),
      ),
      onPressed: () {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => Login()),
            ModalRoute.withName('/'));
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      content: Text(
        "คุณต้องการที่จะออกจากระบบใช่หรือไม่ ?",
        style: TextStyle(fontFamily: 'Athiti'),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final fontB = TextStyle(
      fontFamily: 'Athiti',
      color: Colors.white,
      fontSize: 17,
    );
    final fonttab =
        TextStyle(color: Colors.black, fontFamily: 'Athiti', fontSize: 18);
    var tabBarItem = new TabBar(
      tabs: [
        new Tab(
          child: Text('หน้าหลัก', style: fonttab),
        ),
        new Tab(
          child: Text('โปรโมชั่น', style: fonttab),
        ),
      ],
      controller: tabController,
      indicatorColor: Colors.white,
      unselectedLabelColor: Colors.grey,
    );
    var profile = new Scaffold(
        backgroundColor: Color(0xff00aeac),
        body: SingleChildScrollView(
          child: Column(children: [
            Row(children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Text(
                      ' ชื่อ : ${name == null ? '-' : name}',
                      style: fontB,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Text(
                      ' นามสกุล : ${surname == null ? '-' : surname}',
                      style: fontB,
                    ),
                  ],
                ),
              ),
            ]),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Text(
                    'รหัสสมาชิก : $memNo',
                    style: fontB,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Text(
                    'เบอร์โทรศัพท์มือถือ : ${mobileNo == null ? '-' : mobileNo}',
                    style: fontB,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Text(
                    'คะแนน : ${point == null ? '-' : point}',
                    style: fontB,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Text(
                    'App ID : ${appId == null ? '-' : appId}',
                    style: fontB,
                  ),
                ],
              ),
            ),
          ]),
        ));
    var promotion = new Scaffold(
      backgroundColor: Color(0xff00aeac),
      body: GridView.builder(
        itemCount: lenWeb == null ? 0 : lenWeb,
        scrollDirection: Axis.vertical,
        controller: ScrollController(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        ),
        itemBuilder: (BuildContext context, int index) {
          return Container(
            child: InkWell(
                onTap: () {
                  link = map[index]['link'];
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => WebPage(linkm: link)),
                  );
                },
                child: Card(
                  elevation: 9,
                  child: new GridTile(
                    child: Center(
                        child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  '${map[index]['title']}',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: 'Athiti', fontSize: 18),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    )),
                  ),
                )),
          );
        },
      ),
    );
    return new DefaultTabController(
      length: 2,
      child: new Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.logout,
                color: Color(0xff00aeac),
              ),
              onPressed: () {
                showAlertDialog(context);
              },
            )
          ],
          automaticallyImplyLeading: false,
          bottom: tabBarItem,
          title: Center(
              child: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Image.asset("assets/img/OP.jpeg",
                fit: BoxFit.cover, width: 230),
          )),
          backgroundColor: Colors.white,
        ),
        body: new TabBarView(
          physics: NeverScrollableScrollPhysics(),
          controller: tabController,
          children: <Widget>[profile, promotion],
        ),
      ),
    );
  }
}
